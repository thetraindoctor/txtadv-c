//#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>
#include <SDL2/SDL_timer.h>

#include "cxxopts.hpp"

/*cxxopts::Options arguments("txtadv -p char -l int -c int", "");

void initargs() {
    arguments.add_options("Standard args")
            ("h,help", "Print help")
            ("p,player", "Specify char used for player character", cxxopts::value<char>()
                ->default_value("@")->implicit_value("@"))
            ("l,line", "initial y starting value", cxxopts::value<int>()
                ->default_value("1")->implicit_value("1"))
            ("c,column", "initial x starting value", cxxopts::value<int>()
                ->default_value("1")->implicit_value("1"))
        ;
}*/

int main(int argc, char* argv[]) {

	// attempt to initialize graphics and timer system
    if (SDL_Init(SDL_INIT_TIMER) != 0)
    {
        printf("error initializing SDL: %s\n", SDL_GetError());
        return 1;
    }

    // wait a few seconds
    SDL_Delay(5000);
    
    // clean up resources before exiting
	SDL_Quit();

	return 0;
}
